#include "Mpu6050.h"
#include "esp32_i2c_rw.h"
#include "Mpu6050_registers.h"

// Address of MPU6050 (Can be 0x68 or 0x69):
#define MPU6050_ADDRESS_LOW     0x68 // Address pin low (GND).
#define MPU6050_ADDRESS_HIGH    0x69 // Address pin high (VCC).
#define MPU6050_DEVICE          MPU6050_ADDRESS_LOW

#define I2C_SDA         21
#define I2C_SCL         22
#define I2C_FREQ        400000
#define I2C_PORT        I2C_NUM_0

void CMpu6050::Init(void)
{
    // i2c init
    i2c_config_t conf = {};
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = I2C_SDA;
    conf.scl_io_num = I2C_SCL;
    //conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    //conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = I2C_FREQ;
    i2c_param_config(I2C_PORT, &conf);

    ESP_ERROR_CHECK(i2c_driver_install(I2C_PORT, I2C_MODE_MASTER, 0, 0, 0));

    // mpu6050 init
    mDeviceAddress = MPU6050_DEVICE;
    SetClockSource(MPU6050_CLOCK_PLL_XGYRO);
    SetFullScaleRotationRange(MPU6050_GYRO_FULL_SCALE_RANGE_250);
    SetFullScaleAccelRange(MPU6050_ACCEL_FULL_SCALE_RANGE_4);
    SetSleepEnabled(0);
}

void CMpu6050::SetClockSource(uint8_t source)
{
    esp32_i2c_write_bits(mDeviceAddress, MPU6050_REGISTER_PWR_MGMT_1, MPU6050_PWR1_CLKSEL_BIT, MPU6050_PWR1_CLKSEL_LENGTH, source);
}

void CMpu6050::SetFullScaleRotationRange(uint8_t range)
{
    esp32_i2c_write_bits(mDeviceAddress, MPU6050_REGISTER_GYRO_CONFIG, MPU6050_GCONFIG_FS_SEL_BIT, MPU6050_GCONFIG_FS_SEL_LENGTH, range);
}

void CMpu6050::SetFullScaleAccelRange(uint8_t range)
{
    esp32_i2c_write_bits(mDeviceAddress, MPU6050_REGISTER_ACCEL_CONFIG, MPU6050_ACONFIG_AFS_SEL_BIT, MPU6050_ACONFIG_AFS_SEL_LENGTH, range);
}

void CMpu6050::SetSleepEnabled(bool enabled)
{
    esp32_i2c_write_bit(mDeviceAddress, MPU6050_REGISTER_PWR_MGMT_1, MPU6050_PWR1_SLEEP_BIT, enabled);
}
