#ifndef _ADS8028_hf
#define _ADS8028_hf

#include "driver/spi_master.h"
#include "IAnalogDigitalConverter.h"

//DATA WRITE OPERATION  
#define WRITE 0x8000      // Write enabled: the next 15 bits update the Control Register
#define REPEAT 0x4000     // Enable repeat conversion mode  
#define AIC0 0x2000       // AIN channel 0 is selected for conversion
#define AIC1 0x1000       // AIN channel 1 is selected for conversion
#define AIC2 0x0800       // AIN channel 2 is selected for conversion
#define AIC3 0x0400       // AIN channel 3 is selected for conversion
#define AIC4 0x0200       // AIN channel 4 is selected for conversion
#define AIC5 0x0100       // AIN channel 5 is selected for conversion
#define AIC6 0x0080       // AIN channel 6 is selected for conversion
#define AIC7 0x0040       // AIN channel 7 is selected for conversion
#define TEMP_SENSE 0x0020 // Internal temperature sensor output is selected for conversion 
#define EXT_REF 0x0004    // External reference is used for the next conversion
#define TMP_AVG 0x0002    // Averaging is enabled on the temperature sensor result
#define STANDBY 0x0001    // The ADS8028 goes to standby mode in the next cycle

class CAds8028 : public IAnalogDigitalConverter
{
public:
    CAds8028(void) : IAnalogDigitalConverter() {}
    virtual ~CAds8028(void) {}

    // Initialisierung des Chips
    virtual void Init(void);

    // Auslesen der gezaehlten Inkremente
    virtual unsigned int ReadAdc0(void);

    // ADC in Standby Modus (1.5mA) versetzen
    void EnterStandby();

    // ADC von Standby Modus (1.5mA) in normalen Modus (5.1mA) versetzen
    void ExitStandby();

protected:
    spi_device_handle_t mSpiDeviceHdl;
};

#endif // #ifndef _ADS8028_hf
