#include "Ads8028.h"
#include <stdlib.h>
#include <string.h>
#include <cstdint>
#include <esp_system.h>
#include <esp_err.h>
#include "driver/gpio.h"
#include "esp32/rom/ets_sys.h"
#include "../../Spi/SpiBusHandler.h"

#define PIN_NUM_CS 32
#define VREF 3.3

void CAds8028::Init()
{
    spi_device_interface_config_t spi_device_cfg = {
        .command_bits = 0,
        .address_bits = 0,
        .mode = 0,
        .cs_ena_pretrans = 1,
        .clock_speed_hz = 1*1000*1000,
        .spics_io_num = PIN_NUM_CS,
        .queue_size = 7
    };
    CSpiBusHandler::GetInstance()->AddDevice(spi_device_cfg, mSpiDeviceHdl);
}

unsigned int CAds8028::ReadAdc0()
{
    if (mSpiDeviceHdl == NULL) return 0;

    esp_err_t err;
    spi_transaction_t ADC0;
    memset(&ADC0, 0, sizeof(ADC0));
    uint16_t txbufADC0 = SPI_SWAP_DATA_TX((WRITE|AIC0|EXT_REF),16);
    ADC0.length = 16;
    ADC0.tx_buffer = &txbufADC0;
    ADC0.rx_buffer = NULL;
    err = spi_device_polling_transmit(mSpiDeviceHdl, &ADC0);
    ESP_ERROR_CHECK(err);

    txbufADC0 = 0x0000;
    err = spi_device_polling_transmit(mSpiDeviceHdl, &ADC0);
    ESP_ERROR_CHECK(err);

    ADC0.flags = SPI_TRANS_USE_RXDATA;
    ADC0.rxlength = 16;
    err = spi_device_polling_transmit(mSpiDeviceHdl, &ADC0);
    ESP_ERROR_CHECK(err);
    uint16_t databuf = SPI_SWAP_DATA_RX(*(uint16_t*)ADC0.rx_data,16);
    
    constexpr std::uint_fast16_t maskData {0b0000111111111111};
    databuf &= maskData;

    return static_cast<unsigned int>(databuf);
}

void CAds8028::EnterStandby()
{
    if (mSpiDeviceHdl == NULL) return;

    esp_err_t err;
    spi_transaction_t temp;
    memset(&temp, 0, sizeof(temp));
    uint16_t txbuftemp = SPI_SWAP_DATA_TX((WRITE|STANDBY),16);
    temp.length = 16;
    temp.tx_buffer = &txbuftemp;
    temp.rx_buffer = NULL;
    err = spi_device_transmit(mSpiDeviceHdl, &temp);
    ESP_ERROR_CHECK(err);
}

void CAds8028::ExitStandby()
{
    if (mSpiDeviceHdl == NULL) return;

    esp_err_t err;
    spi_transaction_t temp;
    memset(&temp, 0, sizeof(temp));
    uint16_t txbuftemp = SPI_SWAP_DATA_TX((WRITE),16);
    temp.length = 16;
    temp.tx_buffer = &txbuftemp;
    temp.rx_buffer = NULL;
    err = spi_device_transmit(mSpiDeviceHdl, &temp);
    ESP_ERROR_CHECK(err);
}
