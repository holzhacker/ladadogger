#ifndef _IANALOGDIGITALCONVERTER_hf
#define _IANALOGDIGITALCONVERTER_hf

// Interface des/der ADC's
class IAnalogDigitalConverter
{
public:
    IAnalogDigitalConverter(void) {};
    ~IAnalogDigitalConverter(void) {};

    // Initialisierung
    virtual void Init(void) = 0;
};

#endif // #ifndef _IANALOGDIGITALCONVERTER_hf
