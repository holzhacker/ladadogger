#ifndef _MPU6050_hf
#define _MPU6050_hf

#include <stdint.h>
#include "IAnalogDigitalConverter.h"

class CMpu6050 : public IAnalogDigitalConverter
{
public:
    CMpu6050(void) : IAnalogDigitalConverter() {}
    virtual ~CMpu6050(void) {}

    // Initialisierung
    virtual void Init(void);

protected:
    uint8_t mDeviceAddress;
    uint8_t mBuffer[14];

private:
    void SetClockSource(uint8_t source);
    void SetFullScaleRotationRange(uint8_t range);
    void SetFullScaleAccelRange(uint8_t range);
    void SetSleepEnabled(bool enabled);
};


#endif // #ifndef _MPU6050_hf
