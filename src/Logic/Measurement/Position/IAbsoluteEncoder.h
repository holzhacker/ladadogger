#ifndef _IABSOLUTEENCODER_hf
#define _IABSOLUTEENCODER_hf

#include "IPositionMeasurement.h"

// Interface der Wegmessung mit Absolutwertgeber
class IAbsoluteEncoder : public IPositionMeasurement
{
public:
    IAbsoluteEncoder(void) : IPositionMeasurement() {}
    ~IAbsoluteEncoder(void) {}

    // Initialisierung
    virtual void Init(void) = 0;

    // Aktueller Weg in mm
    virtual double GetPositionMm(void) {return 0;};

private:
    // Auslesen des Analog Digital Konvertors
    virtual unsigned int ReadAdc0(void);
};

#endif // #ifndef _IABSOLUTEENCODER_hf
