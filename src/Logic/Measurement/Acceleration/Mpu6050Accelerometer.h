#ifndef _MPU6050ACCELEROMETER_hf
#define _MPU6050ACCELEROMETER_hf

#include <stdint.h>
#include "IAccelerometer.h"
#include "../Adc/Mpu6050.h"

class CMpu6050Accelerometer : public IAccelerometer, CMpu6050
{
public:
    CMpu6050Accelerometer(void) : IAccelerometer(), CMpu6050() {}
    ~CMpu6050Accelerometer(void) {}

    // Initialisierung
    virtual void Init(void);

    // Aktuelle Beschleunigung
    virtual SAccelerationData GetAcceleration(void);
    
    // Aktuelle Rotation
    virtual SRotationData GetRotation(void);
};


#endif // #ifndef _MPU6050ACCELEROMETER_hf
