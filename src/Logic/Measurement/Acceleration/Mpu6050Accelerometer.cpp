#include "Mpu6050Accelerometer.h"
#include "../Adc/esp32_i2c_rw.h"
#include "../Adc/Mpu6050_registers.h"

void CMpu6050Accelerometer::Init(void)
{
    CMpu6050::Init();
}

SAccelerationData CMpu6050Accelerometer::GetAcceleration(void)
{
    SAccelerationData data;

    esp32_i2c_read_bytes(mDeviceAddress, MPU6050_REGISTER_ACCEL_XOUT_H, 6, mBuffer);
    data.acceleration_x = (((int16_t) mBuffer[0]) << 8) | mBuffer[1];
    data.acceleration_y = (((int16_t) mBuffer[2]) << 8) | mBuffer[3];
    data.acceleration_z = (((int16_t) mBuffer[4]) << 8) | mBuffer[5];

    return data;
}

SRotationData CMpu6050Accelerometer::GetRotation(void)
{
    SRotationData data;

    esp32_i2c_read_bytes(mDeviceAddress, MPU6050_REGISTER_GYRO_XOUT_H, 6, mBuffer);
    data.rotation_x = (((int16_t) mBuffer[0]) << 8) | mBuffer[1];
    data.rotation_y = (((int16_t) mBuffer[2]) << 8) | mBuffer[3];
    data.rotation_z = (((int16_t) mBuffer[4]) << 8) | mBuffer[5];

    return data;
}
