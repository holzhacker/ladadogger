#ifndef _ITEMPERATUREMEASUREMENT_hf
#define _ITEMPERATUREMEASUREMENT_hf

// Interface der Temperaturmessung
class ITemperatureMeasurement
{
public:
    ITemperatureMeasurement(void) {}
    virtual ~ITemperatureMeasurement(void) {}

    // Initialisierung
    virtual void Init(void) = 0;

    // Aktuelle Temperatur in °C
    virtual double GetTemperature(void) = 0;
};

#endif // #ifndef _ITEMPERATUREMEASUREMENT_hf
