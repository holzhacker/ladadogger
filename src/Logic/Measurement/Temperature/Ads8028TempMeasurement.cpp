#include "Ads8028TempMeasurement.h"
#include <stdlib.h>
#include <string.h>
#include <cstdint>
#include "esp_system.h"
#include "esp_err.h"
#include "driver/gpio.h"
#include "esp32/rom/ets_sys.h"

void CAds8028TempMeasurement::Init()
{
    CAds8028::Init();
}

double CAds8028TempMeasurement::GetTemperature()
{
    esp_err_t err;
    spi_transaction_t temp;
    memset(&temp, 0, sizeof(temp));
    uint16_t txbuftemp = SPI_SWAP_DATA_TX((WRITE|TEMP_SENSE|EXT_REF),16);
    temp.length = 16;
    temp.tx_buffer = &txbuftemp;
    temp.rx_buffer = NULL;
    err = spi_device_transmit(mSpiDeviceHdl, &temp);
    ESP_ERROR_CHECK(err);

    txbuftemp = 0x0000;
    err = spi_device_transmit(mSpiDeviceHdl, &temp);
    ESP_ERROR_CHECK(err);

    //30us warten  (wird zwingend benötigt, sonst fehlerhaftes/ ungültiges Ergebnis)
    ets_delay_us(30);
    
    temp.flags = SPI_TRANS_USE_RXDATA;
    temp.rxlength = 16;
    err = spi_device_transmit(mSpiDeviceHdl, &temp);
    ESP_ERROR_CHECK(err); 
    uint16_t databuf = SPI_SWAP_DATA_RX(*(uint16_t*)temp.rx_data,16);

    printf("%d\n", databuf);
    constexpr std::uint_fast16_t maskData {0b0000111111111111};
    databuf &= maskData;
    printf("%d\n", databuf);

    double tempval = (3.1 * (((databuf)/10) + 109.3)) - 273.15;
    printf("%f °C\n",tempval);

    return tempval;
}
