#ifndef _ADS8028TEMPMEASUREMENT_hf
#define _ADS8028TEMPMEASUREMENT_hf

#include "driver/spi_master.h"
#include "ITemperatureMeasurement.h"
#include "../Adc/Ads8028.h"

class CAds8028TempMeasurement : public ITemperatureMeasurement, CAds8028
{
public:
    CAds8028TempMeasurement(void) : ITemperatureMeasurement(), CAds8028() {}
    ~CAds8028TempMeasurement(void) {}

    // Initialisierung des Chips
    virtual void Init(void);

    // Auslesen der Chip Temperatur
    virtual double GetTemperature();
};

#endif // #ifndef _ADS8028TEMPMEASUREMENT_hf
