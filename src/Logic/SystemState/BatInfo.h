#ifndef _BatInfo_hf
#define _BatInfo_hf

float getLipoVoltage(void);
int checkLipoStatus(float adc_reading);
int readVoltage(void);
void initPiezoBuzzer(void);
void tone(unsigned int freq, unsigned int duration);
void SoundStartLog(void);
void SoundStopLog(void);

#endif // #ifndef _BatInfo_hf
