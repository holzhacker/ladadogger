#include "BatInfo.h"

#include <stdlib.h>
#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h" 
#include "driver/adc.h"
#include "driver/gpio.h"
#include "driver/ledc.h"
#include "esp_err.h"
#include "esp_log.h"

#define GPIO_OUTPUT_SPEED LEDC_LOW_SPEED_MODE
#define GPIO_Buzzer 27

static const char* TAG = "Log";

static const float LIPO_FULL = 4.2;
static const float LIPO_NORMAL = 3.5;
static const float LIPO_LOW = 3.3;
static const float LIPO_CRITICAL = 3.1;

int readVoltage()
{
    adc1_config_width(ADC_WIDTH_12Bit);
    adc1_config_channel_atten( ADC1_CHANNEL_7, ADC_ATTEN_11db );
    return adc1_get_raw( ADC1_CHANNEL_7);
}

float getLipoVoltage(void)
{
    //slow but precise read (multisampling)
    //linear voltage range from 0.15v to 2.5v
    long adc_sum = 0;
    float voltage;
    
    for(int i=0; i<=10; i++)
    {   
        adc_sum += readVoltage();
    }
    voltage = 0.0008*(adc_sum/10);
    voltage = 4.2 * voltage / 2.48;
    return voltage;
}

int checkLipoStatus(float adc_reading)
{
    int lipo_status = 1;
    ESP_LOGI(TAG, "Measured lipo Voltage: %f", adc_reading);
    float voltage = adc_reading;
    if(voltage < LIPO_FULL)
    {
        
        lipo_status = 0;
    }
    else if(voltage < LIPO_NORMAL)
    {
        
        lipo_status = 0;
    }
    else if(voltage < LIPO_LOW)
    {
        
        lipo_status = 0;
    }
    else if (voltage < LIPO_CRITICAL)
    {
        ESP_LOGI(TAG, "Lipo at critical undervoltage!");
        lipo_status = 1;    
    }
    return lipo_status;
}

void initPiezoBuzzer()
{
    ledc_channel_config_t ledc_config;
    ledc_config.gpio_num = GPIO_Buzzer;
    ledc_config.speed_mode = GPIO_OUTPUT_SPEED;
    ledc_config.channel = LEDC_CHANNEL_0;
    ledc_config.intr_type = LEDC_INTR_DISABLE;
    ledc_config.timer_sel = LEDC_TIMER_0;
    ledc_config.duty = 0x0;
    ledc_channel_config(&ledc_config);

    ledc_timer_config_t timer_config;
    timer_config.speed_mode = GPIO_OUTPUT_SPEED; 
    timer_config.duty_resolution = LEDC_TIMER_10_BIT;
    timer_config.timer_num = LEDC_TIMER_0;
    timer_config.freq_hz = 1000;
    timer_config.clk_cfg = LEDC_USE_APB_CLK;
    ledc_timer_config(&timer_config);
}

void tone(unsigned int freq, unsigned int duration)
{
    //Start
    ledc_set_freq(GPIO_OUTPUT_SPEED, LEDC_TIMER_0, freq);
    ledc_set_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0, 511);
    ledc_update_duty(GPIO_OUTPUT_SPEED,LEDC_CHANNEL_0);

    vTaskDelay(duration/portTICK_PERIOD_MS);

    //Stop
    ledc_set_duty(GPIO_OUTPUT_SPEED, LEDC_CHANNEL_0, 0);
    ledc_update_duty(GPIO_OUTPUT_SPEED,LEDC_CHANNEL_0);
}

void SoundStartLog()
{
    tone(523,150);
    tone(660,150);
    tone(783,150);
}

void SoundStopLog()
{   
    tone(783,150);
    tone(660,150);
    tone(523,150);
}
