#include "tFast.h"

#include <atomic>
#include <stdlib.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"

#include "Logic/DataLogStateMachine.h"
#include "Tasks/tasks.h"

void tFast(void* pvParameters)
{
    // Initialise the xLastWakeTime variable with the current time.
    TickType_t xLastWakeTime = xTaskGetTickCount();
    const TickType_t xFrequency = 1 / portTICK_PERIOD_MS; // 1ms 

    flagrestart-=restart_fast;
    while (flagrestart != 0) {}
    for( ;; )
    {
        // Wait for the next cycle.
        vTaskDelayUntil( &xLastWakeTime, xFrequency );

        // Perform action here.
        CDataLogStateMachine::GetInstance()->Send();
    }
}
