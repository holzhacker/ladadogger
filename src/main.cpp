#include <atomic>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "Tasks/tFast.h"
#include "Tasks/tLog.h"
#include "Tasks/tasks.h"

#include "esp_log.h"


TaskHandle_t Task1, Task2 = NULL;
std::atomic<int> flagrestart = ATOMIC_VAR_INIT(restart_fast|restart_log);

extern "C" int app_main(void)
{
    xTaskCreatePinnedToCore(tFast, "tFast", 4098*8, NULL, 1, &Task1, 1);
    xTaskCreatePinnedToCore(tLog, "tLog", 4098*8, NULL, 2, &Task2, 0);

    return EXIT_SUCCESS;
}
